<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;


class InsertColors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('colors')->insert([
            ['colorName' => 'bleu'],
            ['colorName' => 'rouge'],
            ['colorName' => 'jaune'],
            ['colorName' => 'vert'],
            ['colorName' => 'orange'],
            ['colorName' => 'violet'],
            ['colorName' => 'rose'],
            ['colorName' => 'gris'],
            ['colorName' => 'blanc']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
