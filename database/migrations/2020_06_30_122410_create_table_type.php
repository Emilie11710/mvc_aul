<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateTableType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_of_users', function (Blueprint $table) {
            $table->id();
            $table->string('typeOfUser');
            $table->timestamps();
        });
        DB::table('type_of_users')->insert(
            array(
                'typeOfUser' => 'Acheteur',
            )
        );
        DB::table('type_of_users')->insert(
            array(
                'typeOfUser' => 'Vendeur',
            )
        );
        DB::table('type_of_users')->insert(
            array(
                'typeOfUser' => 'Reproducteur',
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('typeOfUsers');
    }
}
