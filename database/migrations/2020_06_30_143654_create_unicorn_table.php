<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnicornTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colors', function (Blueprint $table){
           $table->id();
           $table->string('colorName'); 
        });
        Schema::create('destinations', function (Blueprint $table){
            $table->id();
            $table->string('destination');
        });
        Schema::create('unicorns', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->boolean('gender');
            $table->integer('taille');
            $table->bigInteger('color1_id')->unsigned()->nullable();
            $table->foreign('color1_id')->references('id')->on('colors')->onDelete('SET NULL');
            $table->bigInteger('color2_id')->unsigned()->nullable();
            $table->foreign('color2_id')->references('id')->on('colors')->onDelete('SET NULL');
            $table->bigInteger('destination_id')->unsigned()->nullable();
            $table->foreign('destination_id')->references('id')->on('destinations')->onDelete('SET NULL');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('SET NULL');
            $table->float('prix');
            $table->string('description');
            $table->string('photo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unicorns');
        Schema::dropIfExists('colors');
        Schema::dropIfExists('destinations');

    }
}
