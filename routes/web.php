<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/user/{id}/edit', 'UserController@edit')->name('user.edit');
Route::put('/user/{id}', 'UserController@update')->name('user.update');

Route::get('/TypeOfUser', 'TypeOfUserController@index')->name('typeOfUser.index');

Route::get('/unicorn', 'UnicornController@index')->name('unicorn.index');
Route::get('/unicorn/show', 'UnicornController@showByUser')->name('unicorn.show');
Route::get('/unicorn/create', 'UnicornController@create')->name('unicorn.create');
Route::get('/unicorn/{id}/edit', 'UnicornController@edit')->name('unicorn.edit');
Route::post('/unicorn', 'UnicornController@store')->name('unicorn.store');
Route::put('/unicorn/{id}', 'UnicornController@update')->name('unicorn.update');
Route::delete('/unicorn', 'UnicornController@destroy')->name('unicorn.destroy');