<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Color;
use App\Models\Destination;
use App\Models\Unicorn;
use App\User;

class UnicornController extends Controller
{
    public function create()
    {
        $colors = Color::all();
        $destinations = Destination::all();
        $userId = Auth::id();
        return view('unicorn.create', compact('colors', 'destinations', 'userId'));
    }

    public function store(Request $request)
    {
        $unicorn = new Unicorn();
        $unicorn->name = $request->get('name');
        $unicorn->gender = $request->get('gender');
        $unicorn->taille = $request->get('taille');
        $unicorn->color1_id = $request->get('color1_id');
        $unicorn->color2_id = $request->get('color2_id');
        $unicorn->destination_id = $request->get('destination_id');
        $unicorn->user_id = $request->get('user_id');
        $unicorn->prix = $request->get('prix');
        $unicorn->description = $request->get('description');
        $unicorn->photo = $request->get('photo');

        $unicorn->save();
        
        return redirect()->route('unicorn.show');
    }

    public function showByUser()
    {
        $userId = Auth::id();
        $user = User::where('id', $userId)->with('unicorns')->first();
       // dd($user);
        return view('unicorn.show', compact('user'));
    }

    public function index()
    {
        $unicorns = Unicorn::all();

        return view('unicorn.index', compact('unicorns'));
    }

    public function edit($id)
    {
        
        $unicorn = Unicorn::find($id);
        return view('unicorn.edit', compact('unicorn'));
    }

    public function update(Request $request, $id)
    {
        $unicorn = Unicorn::find($id);
        $unicorn->name = $request->get('name');
        $unicorn->gender = $request->get('gender');
        $unicorn->taille = $request->get('taille');
        $unicorn->color1_id = $request->get('color1_id');
        $unicorn->color2_id = $request->get('color2_id');
        $unicorn->destination_id = $request->get('destination_id');
        $unicorn->user_id = $request->get('user_id');
        $unicorn->prix = $request->get('prix');
        $unicorn->description = $request->get('description');
        $unicorn->photo = $request->get('photo');

        $unicorn->save();
        
        return redirect()->route('unicorn.edit');
    }

    public function destroy(Request $request)
    {
        $unicorn = Unicorn::find($request->get('id'));
        $unicorn->delete();

        return redirect()->route('unicorn.show');
    }
}