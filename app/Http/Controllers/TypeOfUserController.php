<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\TypeOfUser;

class TypeOfUserController extends Controller
{

    public function index()
    {
        $types = TypeOfUser::all();

        return view('typeOfUser.index', compact('types'));
    }

    
}