<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    protected $fillable = [
        'destination'
    ];

    public function unicorns()
    {
        return $this->hasMany(Unicorn::class);
    }
}
