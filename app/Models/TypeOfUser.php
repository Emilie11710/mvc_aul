<?php

namespace App\Models;
use App\Traits\Enums;

use Illuminate\Database\Eloquent\Model;

class TypeOfUser extends Model
{
    protected $fillable = [
        'typeOfUser'
    ];

    public function users()
    {
      

        return $this->hasMany(User::class);
    }

}
