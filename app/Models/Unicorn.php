<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Unicorn extends Model
{
    protected $fillable = [
        'name', 'gender', 'taille', 'color1_id', 'color2_id', 'destination_id', 'user_id', 'prix', 'description', 'photo'
    ];

    public function color1()
    {
        return $this->belongsTo(Color::class, 'color1_id');
    }
    public function color2()
    {
        return $this->belongsTo(Color::class, 'color2_id');
    }
    public function destination()
    {
        return $this->belongsTo(Destination::class, 'destination_id');
    }
    public function user(){
        return $this->belongsTo(User::Class, 'user_id');
    }
}
