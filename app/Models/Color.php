<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $fillable = [
        'colorName'
    ];

    public function unicorns()
    {
        return $this->hasMany(Unicorn::class);
    }
}
