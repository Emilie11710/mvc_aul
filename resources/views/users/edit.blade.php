@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Modification de mes parametres</div>

                <div class="card-body">

                    <a href="{{ route('home') }}" class="btn btn-danger" title="Retour a la home">Retour à la page d'accueil</a>
                    <form action="{{ route('categories.update', $category->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <label for="name">Nom</label>
                        @if(!is_null($user->name))
                            <input id="name" type="text" name="name" value="{{ $user->name }}">
                        @else
                            <input id="name" type="text" name="name">
                        @endif
                        <label for="typeOfUser">De quelle catégorie êtes vous</label>
                        <input id="typeOfUser">
                        <br>
                        <button type="submit">Envoyer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection