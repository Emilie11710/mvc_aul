@extends('layouts.app')

@section('content')
<div class="container">
    <div class="justify-content-center">
        
            <h1>Vous n'avez pas encore renseigné votre statut</h1>

            <a type="button" class="btn btn-outline-secondary" href="{{ route('typeOfUser.index') }}">Cliquez ici pour choisir votre statut</a>
        
    </div>
</div>
@endsection
