@extends('layouts.app')

@section('content')

<h1>Choisissez votre statut : </h1>
    @foreach($types as $type)
    <form action="" method="POST">
        @csrf
        <div class="form-check">
        
            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
            <label class="form-check-label" for="exampleRadios1">{{ $type->typeOfUser }}</label>
        </div>  
   @endforeach
        <button type="submit" class="btn btn_outline-secondary">Valider</button>
    </form>
    


@endsection