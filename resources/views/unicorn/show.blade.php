@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <h1>Mes licornes</h1>
    <div class="row row-cols-6">
        @if(!is_null ($user->unicorns))
        @foreach($user->unicorns as $unicorn)
        <div class="card m-2" style="width: 18rem;">
            <img src="{{ $unicorn->photo }}" class="card-img-top" alt="Licorne">
            <div class="card-body">
                <h5 class="card-title">{{ $unicorn->name }}</h5>
                @if($unicorn->gender == 0)
                <p class="card-text">Mâle</p>
                @else
                <p class="card-text">Femelle</p>
                @endif
                <a href="{{ route('unicorn.edit', $unicorn->id) }}" class="btn btn-primary">Voir en détail</a>
            </div>
        </div> 
        @endforeach
        @endif
    </div>
</div>


@endsection