@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Ajout d'une licorne à ma collection</div>

                <div class="card-body">


                    <form action="{{ route('unicorn.store') }}" method="POST">
                        <div class="form-group">
                            @csrf
                            <label for="name">Nom</label>
                            <input  class="form-control" id="name" type="text" name="name">

                            <label for="gender">Sexe</label>
                            <select class="form-control" name="gender" id="gender">
                                <option value="0">Mâle</option>
                                <option value="1">Femelle</option>
                            </select>

                            <label for="taille">Taille</label>
                            <input class="form-control" id="taille" type="text" name="taille">

                            <label for="color1_id">Couleur principale</label>
                            <select class="form-control" name="color1_id" id="color1_id">
                            @foreach($colors as $color)
                                <option value="{{ $color->id }}">{{ $color->colorName }}</option>
                            @endforeach
                            </select>

                            <label for="color2_id">Couleur secondaire</label>
                            <select class="form-control" name="color2_id" id="color2_id">
                            @foreach($colors as $color)
                                <option value="{{ $color->id }}">{{ $color->colorName }}</option>
                            @endforeach
                            </select>
                
                            <label for="destination_id">Destination</label>
                            <select class="form-control" name="destination_id" id="destination_id">
                            @foreach($destinations as $destination)
                                <option value="{{ $destination->id }}">{{ $destination->destination }}</option>
                            @endforeach
                            </select>
                
                            <input class="form-control" id="user_id" value="{{ $userId }}" type ="hidden" name="user_id">

                            <label for="prix">Prix</label>
                            <input class="form-control" id="prix" type="text" name="prix">

                            <label for="description">Description</label>
                            <input class="form-control" id="description" type="text" name="description">

                            <label for="photo">Photo</label>
                            <input class="form-control" id="photo" type="text" name="photo">
                            <br>
                        </div>
                        <button type="submit" class="btn btn-outline-secondary">Envoyer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection