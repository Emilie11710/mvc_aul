@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div>
        <div class="card m-2" style="width: auto;">
           <div class="row">
                <img class="col-2" src="{{ $unicorn->photo }}" class="card-img-top" alt="Licorne">
                <div class="col-10">
                    <h5 class="card-title">{{ $unicorn->name }}</h5>
                    @if($unicorn->gender == 0)
                    <p class="card-text">Mâle</p>
                    @else
                    <p class="card-text">Femelle</p>
                    @endif
                    <p>{{ $unicorn->description }}</p>
                    <p>{{ $unicorn->taille }} cm</p>
                    <p>Couleur principale : {{ $unicorn->color1_id }}</p>
                    <p>Couleur secondaire : {{ $unicorn->color1_id }}</p>
                    <p>{{ $unicorn->prix }} euros</p>

                    <a href="#" class="btn btn-warning">Modifier la fiche</a>
                    <a href="{{ route('unicorn.destroy', $unicorn->id) }}" class="btn btn-danger">Supprimer la fiche</a>

                </div>
            </div>
        </div> 

    </div>
</div>


@endsection